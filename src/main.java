import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class main {
    public static void main (String[] args) throws IOException
    {

        WordFinder wf = new WordFinder("C:\\Users\\Hinson.christopher\\IdeaProjects\\pittprg2\\src\\dictionary.txt");
        Scanner k = new Scanner(System.in);

        Boolean running = Boolean.TRUE;

        System.out.println(
                "Welcome to Anagrams, Here's how to play: \n" +
                        "    You will see a randomly selected word and in 60 seconds \n" +
                        "    you must find as many dictionary-valid words using the \n" +
                        "    characters of the random word as possible. You do not \n" +
                        "    have to use all the letters, but your score for a word \n" +
                        "    equals the number of characters it contains, so longer \n" +
                        "    is better ");

        System.out.println("would you like to play?  (Y/n)");

        //setting up our variables for the session
        int totalScore=0;
        int rounds = 0;
        int wordsFound = 0;

        Boolean returningUser = Boolean.FALSE;
        ArrayList<Integer> userStatsLifetime = new ArrayList<Integer>();
        int userid = 0;

        //main game loop
        while (running)
        {
            String userResponse = k.next();
            if (userResponse.equals("Y")||userResponse.equals("y"))
            {

///////////////////loading previous stats///////////////////////////////////////////////////////////////////////////////

                System.out.println("Would you like to load your previous statistics? (Y)");
                userResponse = k.next();



                if (userResponse.equals("Y")||userResponse.equals("y"))
                {
                    Boolean done = Boolean.FALSE;
                    while (!done)
                    {
                        System.out.println("Please enter your 6-digit identification code");
                        int id = k.nextInt();
                        userid = id;
                        try {

                            File userStats = new File("H:\\anagramUsers\\" + id + ".txt");
                            Scanner userFileRead = new Scanner(userStats);
                            int i = 0;
                            while (userFileRead.hasNextLine())
                            {
                                userStatsLifetime.add(userFileRead.nextInt());
                                i++;
                            }

                            System.out.println("User's lifetime stats: \n" +
                                    "      Rounds Played: " + userStatsLifetime.get(0) +
                                    "\n      Words Found : "+ userStatsLifetime.get(1) +
                                    "\n      Points Scored : "+ userStatsLifetime.get(2));


                            returningUser= Boolean.TRUE;


                            done = Boolean.TRUE;


                        }
                        catch (FileNotFoundException e)
                        {
                            System.out.println("Sorry we cannot find the specified user, " +
                                    "would you like to try again? (Y)");
                            userResponse = k.next();

                            if (userResponse.equals("Y"))
                            {
                                done = Boolean.FALSE;
                            }
                            else
                            {
                                done = Boolean.TRUE;
                            }

                        }
                    }


                }


////////////////beginning of gameplay//////////////////////////////////////////////////////////////////////////////////

                //setting up our variables for the round
                ArrayList<String> userGoodResponses = new ArrayList<String>();

                MyTimer t = new MyTimer();

                int score = 0;




                //set timer time
                t.set(60000);

                //get random word
                wf.nextWord(7);

                //get player ready
                System.out.println("Word is : " + "\u001b[31m"  + wf.showWord() + "\u001b[0m");
                System.out.println("Timing will start when you enter your first word");

                //start timer
                t.start();

                //main game code
                while (t.check())
                {
                    //get user's guess
                    String userGuess = k.next();

                    //determine if valid word
                    Boolean valid = wf.goodWord(userGuess);

                    //make sure timer hasn't expired before giving points
                    if (t.check())
                    {
                        //if valid word and not already used, give point
                        if (valid && !userGoodResponses.contains(userGuess))
                        {
                            userGoodResponses.add(userGuess);
                            System.out.println("That's a valid answer you have received " + userGuess.length() + " points");
                            score = score + userGuess.length();
                        }
                        //if valid word but previously used, do not give points and tell the user
                        else if (valid && userGoodResponses.contains(userGuess))
                        {
                            System.out.println("Thats a valid response, but you've already used that word.  " +
                                    "No points have been awarded");
                        }
                        //if not a valid word, tell the user
                        else
                        {
                            System.out.println("Sorry, that's not a valid response");
                        }
                    }
                    //tell user when their time has elapsed
                    else
                        System.out.println("Sorry, time is up!");
                }

                //print round stats
                System.out.println("You have received " +"\u001b[36m" +score + "\u001b[0m"  +
                        " points after playing the following " + "\u001b[36m" + userGoodResponses.size() + "\u001b[0m"
                        + " words: ");
                for (int i =0;i<userGoodResponses.size();i++)
                {
                    System.out.println(userGoodResponses.get(i));
                }

                //add to our session variables
                totalScore = totalScore + score;
                rounds++;
                wordsFound = wordsFound + userGoodResponses.size();

                //add to our lifetime variables

                if (returningUser)
                {
                    userStatsLifetime.set(0, userStatsLifetime.get(0) + 1);
                    userStatsLifetime.set(1, userStatsLifetime.get(1) + wordsFound);
                    userStatsLifetime.set(2, userStatsLifetime.get(2) + score);
                }
                //ask to start another round or not
                System.out.println("\n Would you like to play again? (Y/n)");


            }
            else
            {

                //insult the user if they're chosen to exit without playing
                if (rounds == 0)
                {
                    System.out.println("You didn't even try, now that's just sad :(");
                    System.out.println("See you next time i guess :/");
                }
                else
                {
                    //if the user has played >0 rounds, print their session stats
                    System.out.println("You played " + rounds + " rounds");
                    System.out.println("Finding " + wordsFound + " words");
                    System.out.println("Earning a total of " + totalScore + " points");
                    System.out.println("That works out to be an average of " + (double)(wordsFound/rounds) + " words " +
                            "found per round; for an average points per round of " + (double) (totalScore/rounds));

                    if (returningUser)
                    {
                        System.out.println("\n\n Over your lifetime: ");
                        System.out.println("You have played " + userStatsLifetime.get(0) + " rounds");
                        System.out.println("Finding " + userStatsLifetime.get(1) + " words");
                        System.out.println("Earning a total of " + userStatsLifetime.get(2) + " points");
                        System.out.println("That works out to be an average of " + (double)(userStatsLifetime.get(1)/userStatsLifetime.get(0)) + " words " +
                                "found per round; for an average points per round of " + (double) (userStatsLifetime.get(2)/userStatsLifetime.get(0)));
                    }
                    else
                    {
                        System.out.println("Would you like to save these stats for the future? (Y/n)");
                        userResponse = k.next();
                        if (userResponse.equals("Y")||userResponse.equals("y"))
                        {
                            System.out.println("please enter your 6 digit id");
                            userid = k.nextInt();
                            userStatsLifetime.add(rounds);
                            userStatsLifetime.add(wordsFound);
                            userStatsLifetime.add(totalScore);

                            System.out.println("Thank you for registering.  Remember your 6 digit code as you will " +
                                    "need it to access scores in the future");
                        }
                    }


/////////////////////Writing user's lifetime details back to their file/////////////////////////////////////////////////

                    Writer wr = new FileWriter("H:\\anagramUsers\\" + userid + ".txt");
                    wr.write(userStatsLifetime.get(0).toString());
                    wr.write("\n");
                    wr.write(userStatsLifetime.get(1).toString());
                    wr.write("\n");
                    wr.write(userStatsLifetime.get(2).toString());
                    wr.close();


                    System.out.println("See you next time!");
                }




                running = Boolean.FALSE;
            }
        }

    }
}
