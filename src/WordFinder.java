// CS 401
// Assignment 2 WordFinder class
// You must complete the implementation of this class.  You will need
// to use an instance of the Dictionary class within this class.  See
// Dictionary.java for details on the Dictionary class.

public class WordFinder
{
	// Think about the instance variables that you will need for this class.
	// Minimally you will need a Dictionary and a String.
	private Dictionary dict;
	private String pickedWord;

	// Initialize a WordFinder object.  String fileName is the name of a
	// Dictionary file from which the Dictionary instance variable will be
	// initialized.
	public WordFinder(String fileName)
	{
		//initialize a new Dictionary class using the passed in filename
		dict = new Dictionary(fileName);
	}
	
	// Obtain and store a random word from the Dictionary of "size" or more
	// letters.
	public void nextWord(int size)
	{
			pickedWord = dict.randWord(size);
	}
	
	// Return the word that was obtained. This is necessary since the word itself
	// will be stored in a private instance variable.
	public String showWord()
	{
		return pickedWord;
	}
	
	// This is the most challenging method in this class.  The "test" argument is
	// a String that will be checked for validity within the current word that was
	// obtained from the Dictionary.  This method should return true only if
	//
	// all of the characters in "test" are found within the word (such that each letter in
	// the word is used at most one time) and if
	//
	// "test" is also a valid word in the Dictionary.
	//
	// Think about how you will do this and consult the Java API for
	// some ideas.
	public boolean goodWord(String test)
	{
//////////check first for dictionary valid word, easiest to determine.//////////////////////////////////////////////////
		if (dict.contains(test))
		{
			//don't do anything, just move onto character checking
		}
		else
		{
			//dent bother checking character validity if not a dictionary valid word
			return Boolean.FALSE;
		}

//////////if valid word, check for character validity///////////////////////////////////////////////////////////////////

		//we need StringBuilder versions of the dictionary word and user word so we can access specific indexed chars
		StringBuilder dictWord = new StringBuilder(pickedWord);
		StringBuilder userWord = new StringBuilder(test);

		//this stringbuilder lets us keep track of what's already been found and root out duplicate chars
		StringBuilder alreadyFoundChars = new StringBuilder();

		//iterating over the user's chosen word character by character
		for (int i=0; i<userWord.length(); i++)
		{
			//get the current character we're analyzing
			char currentChar = userWord.charAt(i);

			//we will need this int to determine if no character has been found
			int bleh = alreadyFoundChars.length();

			//then we determine if it's contained in the dictionary word
			for (int b = 0; b<dictWord.length();b++)
			{
				if (dictWord.charAt(b)==currentChar)
				{
					//if it is, add it to the list of already found words and move onto next character
					alreadyFoundChars.append(dictWord.charAt(b));
					//delete the character from the dictionary word so it cant be found again
					dictWord.deleteCharAt(b);
					break;
				}
			}

			//if the character has not been found, the length of alreadyFoundChars will not have changed,so return false
			if (alreadyFoundChars.length() == bleh)
			{
				return Boolean.FALSE;
			}

		}

		return Boolean.TRUE;

	}
}
